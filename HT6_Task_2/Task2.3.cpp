#include <iostream>
#include <exception>
#include <stdexcept>

using namespace std;

class Foo {
public:
    ~Foo() {
        if (uncaught_exception()) {
            cout << "~Foo() called during stack unwinding\n";
        } else {
            cout << "~Foo() called normally\n";
        }
    }
};
int main()
{
    Foo f;
    try {
        Foo f;
        cout << "Exception thrown\n";
        throw runtime_error("test exception");
    } catch (const exception& e) {
        cout << "Exception caught: " << e.what() << '\n';
    }
}
