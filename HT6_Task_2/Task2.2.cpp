#include <iostream>

using namespace std;

class Exception1
{

};

class Exception2
{

};

class Exception3
{

};

void f(int a)
{
    if (a == 1)
        throw Exception1();
    if (a == 2 )
        throw Exception2();
    if (a == 3)
        throw Exception3();
}

void g()
{
    int a;
    cout << "Enter a (1 or 2): ";
    cin >> a;
    try
    {
        f(a);
    }
    catch (const Exception1& e)
    {
        cout << "Exception 1" << endl;
        throw e;
    }
    catch (const Exception2& a)
    {
        cout << "Exception 2" << endl;
        throw a;
    }
    catch (const Exception3&)
    {
        cout << "Exception 3" << endl;
    }

}

int main()
{
    try
    {
        g();
    }
    catch (const Exception3&)
    {
        cout << "Exception 3" << endl;
    }
    catch (...)
    {
        cout << "Exception in main" << endl;
    }
    system("pause");
    return 0;
}
