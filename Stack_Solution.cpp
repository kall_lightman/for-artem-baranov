#include <iostream>

class Stack10 
{
private:
	int number;
	int *i10Stack;
public:

	Stack10() 
	{
		number = 0;
		i10Stack = new int[10];
	};

	Stack10(int value, size_t size) 
	{
		number = 0;
		i10Stack = new int[10];
		for (int i = 0; i < size; ++i) 
		{
			push(value);
		}
	}

	Stack10(const Stack10& a) 
	{
		number = a.number;
		i10Stack = new int[10];
		for (int i = 0; i < a.number; ++i) 
		{
			i10Stack[i] = a.i10Stack[i];
		}
	}


	Stack10(Stack10&& a) : i10Stack(std::move(a.i10Stack)), number(std::move(a.number)) 
	{
		a.i10Stack = nullptr;
		number = 0;
	}

	void push(int elem) 
	{

		if (number == 9) 
		{
			std::cerr << "stack overloading" << std::endl;
		}
		else 
		{
			i10Stack[number] = elem;
			++number;
		}
	}

	int pop()
	{

		if (number == 0) 
		{
			std::cerr << "stack empty" << std::endl;
			return 0;
		}
		else 
		{
			int k = i10Stack[number];
			i10Stack[number] = 0;
			--number;
			return k;
		}
	}
};

int main() 
{
	Stack10 a;
	for (int i = 0; i < 7; ++i) 
	{
		a.push(12 + i * 3);
	}

	for (int i = 0; i < 9; ++i) 
	{
		std::cout << a.pop() << std::endl;
	}
	for (int i = 0; i < 12; ++i) 
	{
		a.push(12 + i * 3);
	}

	Stack10& b = a;
	Stack10 d(12, 7);

	Stack10&& c = Stack10(a);

	for (int i = 0; i < 9; ++i) 
	{
		std::cout << "b: " << b.pop() << std::endl;
	}

	for (int i = 0; i < 9; ++i) 
	{
		std::cout << "c: " << c.pop() << std::endl;
	}

	for (int i = 0; i < 9; ++i) 
	{
		std::cout << "d: " << d.pop() << std::endl;
	}
	//std::cout << "Hello, World!" << std::endl;
	system("pause");
	return 0;
}